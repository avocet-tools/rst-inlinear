package preparser

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"strings"
)

func (p *Preparser) analyze(tok *token.Token) {
	switch tok.Type {
	case token.Text:
		text := []string{p.prime.Text}
		for p.secunde.Type == token.Text {
			text = append(text, p.secunde.Text)
			p.read()
		}
		tok.Init(strings.Join(text, ""))
	case token.Space:
		for p.secunde.Type == token.Space {
			p.read()
		}
	}
}
