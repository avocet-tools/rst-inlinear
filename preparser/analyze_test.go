package preparser

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"testing"
)

func Test_preparser(t *testing.T) {
	name := "rst/inlinear/preparser.Test_preparser"

	data := []struct {
		raw   string
		text  string
		ltype token.Type
	}{
		{"Text", "Text", token.Text},
		{"     ", " ", token.Space},
	}

	for pos, datum := range data {
		p := New("analyze_test.go", datum.raw, 1)
		tok := p.Next()

		if tok.Text != datum.text {
			t.Fatalf(
				"%s %d: preparser failed to set text: %q != %q",
				name, pos, tok.Text, datum.text)
		}

		if tok.Type != datum.ltype {
			t.Fatalf(
				"%s %d: preparser set wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(datum.ltype))
		}
		tok = p.Next()
		if tok.Type != token.EOL {
			t.Fatalf(
				"%s %d: preparser failed to retrieve EOL: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(token.EOL))
		}
	}

}
