// Package preparser provides functions to preparse the results of lexical analyses,
// preparing the tokens for use by the Parser.
package preparser

import (
	"gitlab.com/avocet-tools/inlinear/lexer"
	"gitlab.com/avocet-tools/inlinear/token"
	"gitlab.com/avocet-tools/log"
)

// Preparser provides methods and functions to preparse tokens that result from the
// lexical analysis of reStructuredText.  This is intended to group like tokens to make
// certain patterns clearer to the Parser.
type Preparser struct {
	l       *lexer.Lexer
	prime   *token.Token
	secunde *token.Token

	Trace func(...any)
	Debug func(...any)
	Info  func(...any)
	Warn  func(...any)
	Error func(...any)
	Fatal func(...any)

	Tracef func(string, ...any)
	Debugf func(string, ...any)
	Infof  func(string, ...any)
	Warnf  func(string, ...any)
	Errorf func(string, ...any)
	Fatalf func(string, ...any)
}

// New function takes a filename string, the raw text of the line, and a line number
// and returns a Preparser pointer that is ready for use.
func New(fname, raw string, line int) *Preparser {

	p := &Preparser{
		l: lexer.New(fname, raw, line),
	}
	p.prime = p.l.Next()
	p.secunde = p.l.Next()

	// Configure Loggers
	lgr := log.New("inlinear", "inlinear.preparser")
	p.Trace, p.Tracef = lgr.Trace()
	p.Debug, p.Debugf = lgr.Debug()
	p.Info, p.Infof = lgr.Info()
	p.Warn, p.Warnf = lgr.Warn()
	p.Error, p.Errorf = lgr.Error()
	p.Fatal, p.Fatalf = lgr.Fatal()

	p.Debugf("Instantiate preparser for %s:%d", fname, line)
	return p
}

func (p *Preparser) read() {
	p.prime = p.secunde
	p.secunde = p.l.Next()
}

// Next method retrieves the next token from the lexer and performs the initial stage of syntactic
// analysis, grouping the text from any subsequent tokens and moving the pointer up the next
// genuinely unique token.
func (p *Preparser) Next() *token.Token {
	tok := p.prime
	p.analyze(tok)
	p.read()

	return tok
}
