package inlinear

import "gitlab.com/avocet-tools/log"

var (
	trace func(...any)
	debug func(...any)
	info  func(...any)
	warn  func(...any)
	err   func(...any)
	fatal func(...any)

	tracef func(string, ...any)
	debugf func(string, ...any)
	infof  func(string, ...any)
	warnf  func(string, ...any)
	errorf func(string, ...any)
	fatalf func(string, ...any)
)

func initLogger() {
	lgr := log.New("inlinear", "")
	trace, tracef = lgr.Trace()
	debug, debugf = lgr.Debug()
	info, infof = lgr.Info()
	warn, warnf = lgr.Warn()
	err, errorf = lgr.Error()
	fatal, fatalf = lgr.Fatal()
}
