package inlinear

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/avocet-tools/inlinear/preparser"
	"gitlab.com/avocet-tools/inlinear/token"
)

// RunPreparse executes a preparse operation as called from the command-line, then prints JSON.
func RunPreparse(args []string) {
	initLogger()
	info("Called preparse operation")
	start := time.Now()
	toks := Preparse("stdin", strings.Join(args, " "), 1)

	debug("Marshing Tokens to JSON")
	data, err := json.MarshalIndent(toks, " ", " ")
	if err != nil {
		fatal(err)
	}
	trace("Marshal complete")

	fmt.Println(string(data))
	fmt.Println("Preparse Complete: ", len(toks), " in ", time.Since(start))
}

// Preparse performs lexical analysis and preparses the return tokens.
func Preparse(fname string, raw string, line int) []*token.Token {
	toks := []*token.Token{}
	start := time.Now()

	p := preparser.New(fname, raw, line)
	tok := p.Next()

	for tok.Type != token.EOL {
		toks = append(toks, tok)
		tok = p.Next()
	}
	toks = append(toks, tok)
	debugf("Preparse complete: %d tokens in %s", len(toks), time.Since(start))

	return toks
}
