# RST Inlinear

RST Inlinear provides internal functions and methods used to parse inline elements from
a reStructuredText string.

* [![Build Status](https:/gitlab.com/avocet-tools/rst-inlinear/badges/main/build.svg)](https://gitlab.com/avocet-tools/rst-inlinear/commits/main)

## Install

```
go install gitlab.com/avocet-tools/inlinear
```

## Use

```go
import (
   "gitlab.com/avocet-tools/ast"
   "gitlab.com/avocet-tools/inlinear"
)

func parseRSTString(filename, rawText string, line int) []*ast.Element {
    inlinearParser := inlinear.Parse(filename, rawText, line)
    return inlinearParser.Parse()
}
```

# Configuration

RST Inlinear supports the following configuration options:

| Option | Description |
|---|---|
| **log.debug.inlinear.lexer** | Debugging logs for lexer. |
| **log.debug.inlinear.parser** | Debugging logs for parser. |
| **log.debug.inlinear.preparser** | Debugging logs for preparser. |
| **log.debug.inlinear.prog** | Debugging logs at a program level. |


