package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
)

func (l *Lexer) lexText(tok *token.Token) {
	tok.Type = token.Text
	tok.Init(string(l.prime))
}
