package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"unicode"
)

func (l *Lexer) lexColon(tok *token.Token) {
	if l.secunde == '`' {
		if l.terte == '~' {
			tok.Type = token.ColonTickTilda
			tok.Init(":`~")
			l.read()
			l.read()
		} else {
			tok.Type = token.ColonTick
			tok.Init(":`")
			l.read()
		}
	} else if l.secunde == '/' && l.terte == '/' {
		tok.Type = token.ColonSlashSlash
		tok.Init("://")
		l.read()
		l.read()
	} else if unicode.IsLetter(l.secunde) {
		tok.Type = token.Colon
		tok.Init(":")
	}
}

func (l *Lexer) lexBrace(tok *token.Token) {
	if l.secunde == '`' {
		l.read()
		if l.secunde == '_' && l.terte == '_' {
			tok.Type = token.BraceTickDScore
			tok.Init(">`__")
			l.read()
			l.read()
		} else if l.secunde == '_' {
			tok.Type = token.BraceTickScore
			tok.Init(">`_")
			l.read()
		} else {
			tok.Type = token.BraceTick
			tok.Init(">`")
		}
		return
	}
	tok.Type = token.Text
	tok.Init(">")

}
