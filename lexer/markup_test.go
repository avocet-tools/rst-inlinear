package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"testing"
)

func Test_markup(t *testing.T) {
	name := "rst/inlinear/lexer.Test_markup"
	data := []struct {
		raw   string
		ltype token.Type
	}{
		{"**", token.DStar},
		{"*", token.Star},
		{"[[", token.DOBrack},
		{"[", token.OBrack},
		{"``", token.DTick},
		{"`", token.Tick},
		{"`__", token.TickDScore},
		{"`_", token.TickScore},
		{"]]", token.DCBrack},
		{"]", token.CBrack},
		{"]__", token.CBrackDScore},
		{"]_", token.CBrackScore},
		{"||", token.DBar},
		{"|", token.Bar},
		{"|__", token.BarDScore},
		{"|_", token.BarScore},
	}

	for pos, datum := range data {
		l := New("markup_test.go", datum.raw, 1)
		tok := l.Next()

		if tok.Text != datum.raw {
			t.Fatalf(
				"%s %d: lexer failed to set text: %q != %q",
				name, pos, tok.Text, datum.raw)
		}

		if tok.Type != datum.ltype {
			t.Fatalf(
				"%s %d: lexer set wrong type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(datum.ltype))
		}
	}
}
