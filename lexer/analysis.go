package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
)

func (l *Lexer) analyze(tok *token.Token) {
	switch l.prime {
	case ' ':
		l.lexSpace(tok)
	case '\n':
		l.line++
		l.lexSpace(tok)
	case '*':
		l.lexWithDoub(tok, '*', token.DStar, token.Star)
	case '[':
		l.lexWithDoub(tok, '[', token.DOBrack, token.OBrack)
	case '`':
		l.lexWithScores(tok, '`', token.DTick, token.Tick, token.TickDScore, token.TickScore)
	case ']':
		l.lexWithScores(tok, ']', token.DCBrack, token.CBrack, token.CBrackDScore, token.CBrackScore)
	case '|':
		l.lexWithScores(tok, '|', token.DBar, token.Bar, token.BarDScore, token.BarScore)
	case ':':
		l.lexColon(tok)
	case '>':
		l.lexBrace(tok)
	case 0:
		tok.Type = token.EOL
	default:
		l.lexText(tok)
	}
}
