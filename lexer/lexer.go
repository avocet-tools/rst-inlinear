// Package lexer provides structs and functions used in the inlinear lexical
// analysis of reStructuredText strings.
package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"gitlab.com/avocet-tools/log"
)

// Lexer struct provides methods for inlinerar lexical analysis of
// reStructuredText.
type Lexer struct {
	in      []rune
	readPos int
	pos     int
	size    int

	fname string
	line  int

	prime   rune
	secunde rune
	terte   rune

	// Initialize Logger Functions
	Trace func(...any)
	Debug func(...any)
	Info  func(...any)
	Warn  func(...any)
	Error func(...any)
	Fatal func(...any)

	Tracef func(string, ...any)
	Debugf func(string, ...any)
	Infof  func(string, ...any)
	Warnf  func(string, ...any)
	Errorf func(string, ...any)
	Fatalf func(string, ...any)
}

// New takes a filename, a raw string, and a line number and uses them to
// generate a Lexer pointer.
func New(fname, raw string, line int) *Lexer {
	r := []rune(raw)

	l := &Lexer{
		fname: fname,
		line:  line,
		in:    r,
		size:  len(r),
	}

	// Configure Logging Functions
	lgr := log.New("inlinear", "inlinear:lexer")
	l.Trace, l.Tracef = lgr.Trace()
	l.Debug, l.Debugf = lgr.Debug()
	l.Info, l.Infof = lgr.Info()
	l.Warn, l.Warnf = lgr.Warn()
	l.Error, l.Errorf = lgr.Error()
	l.Fatal, l.Fatalf = lgr.Fatal()

	l.Debugf("Initialized Inlinear Lexer on %s:%d", fname, line)
	return l
}

func (l *Lexer) get(i int) rune {
	if i >= l.size {
		return 0
	}
	return l.in[i]
}

func (l *Lexer) read() {
	l.prime = l.get(l.pos)
	l.secunde = l.get(l.pos + 1)
	l.terte = l.get(l.pos + 2)

	l.pos++
	l.readPos++
}

// Next reads the next rune in the sequence and uses it to instantiate and
// return a token pointer.
func (l *Lexer) Next() *token.Token {
	l.read()
	tok := token.New(l.fname, l.line, l.readPos)

	l.analyze(tok)

	return tok
}
