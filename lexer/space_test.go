package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"testing"
)

func Test_space(t *testing.T) {
	name := "rst/inlinear/lexer.Test_space"

	raw := "  \n  "
	lines := []int{1, 1, 1, 2, 2}

	l := New("space_test.go", raw, 1)

	text := " "
	for pos, line := range lines {
		tok := l.Next()
		tpos := pos + 1

		if tok.Text != text {
			t.Fatalf(
				"%s %d: lexer failed to set text: %q != %q",
				name, pos, tok.Text, text)
		}

		if tok.Line != line {
			t.Fatalf(
				"%s %d: lexer set wrong line number: %d != %d",
				name, pos, tok.Line, line)
		}

		if tok.Pos != tpos {
			t.Fatalf(
				"%s %d: lexer set wrong position: %d != %d",
				name, pos, tok.Pos, tpos)
		}

		if tok.Type != token.Space {
			t.Fatalf(
				"%s %d: lexer set wrong token type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(token.Space))
		}

	}

}
