package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
)

func (l *Lexer) lexSpace(tok *token.Token) {
	tok.Type = token.Space
	tok.Init(" ")
}
