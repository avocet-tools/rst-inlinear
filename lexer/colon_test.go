package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"testing"
)

func Test_colonBrace(t *testing.T) {
	name := "rst/inlinear/lexer.Test_colonBrace"
	data := []struct {
		raw   string
		ltype token.Type
	}{
		{":a", token.Colon},
		{":`", token.ColonTick},
		{":`~", token.ColonTickTilda},
		{"://", token.ColonSlashSlash},
		{">`", token.BraceTick},
		{">`__", token.BraceTickDScore},
		{">`_", token.BraceTickScore},
	}

	for pos, datum := range data {
		l := New("colon_test.go", datum.raw, 1)
		tok := l.Next()

		if tok.Text != datum.raw && tok.Text != ":" {
			t.Fatalf(
				"%s %d: lexer failed to set text: %q != %q",
				name, pos, tok.Text, datum.raw)
		}

		if tok.Type != datum.ltype {
			t.Fatalf(
				"%s %d: lexer failed to set token type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(datum.ltype))
		}
	}
}
