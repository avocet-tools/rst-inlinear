package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
	"strings"
	"testing"
)

func Test_text(t *testing.T) {
	name := "rst/inlinear/lexer.Test_text"
	in := "Text"
	raws := strings.Split(in, "")

	l := New("text_test.go", in, 1)

	for pos, raw := range raws {
		tok := l.Next()

		if tok.Text != raw {
			t.Fatalf(
				"%s %d: lexer failed to set text: %q != %q",
				name, pos, tok.Text, raw)
		}

		if tok.Raw != raw {
			t.Fatalf(
				"%s %d: lexer fialed to set raw string: %q != %q",
				name, pos, tok.Raw, raw)
		}

		if tok.Type != token.Text {
			t.Fatalf(
				"%s %d: lexer failed to set token type: %q != %q",
				name, pos, token.Pretty(tok.Type), token.Pretty(token.Text))
		}
	}
}
