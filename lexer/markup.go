package lexer

import (
	"gitlab.com/avocet-tools/inlinear/token"
)

func stringRune(q ...rune) string {
	rs := append([]rune{}, q...)
	return string(rs)
}

func (l *Lexer) lexWithDoub(
	tok *token.Token, char rune,
	double token.Type,
	base token.Type) {
	if l.secunde == char {
		tok.Text = stringRune(char, char)
		tok.Type = double
		l.read()
	} else {
		tok.Text = string(char)
		tok.Type = base
	}
}

func (l *Lexer) lexWithScores(
	tok *token.Token, char rune,
	dbase token.Type, base token.Type,
	dscore token.Type, score token.Type) {
	if l.secunde == char {
		tok.Text = stringRune(char, char)
		tok.Type = dbase
		l.read()
	} else if l.secunde == '_' {
		if l.terte == '_' {
			tok.Type = dscore
			tok.Text = stringRune(char, '_', '_')
			l.read()
			l.read()
		} else {
			tok.Type = score
			tok.Text = stringRune(char, '_')
			l.read()
		}
	} else {
		tok.Type = base
		tok.Text = string(char)
	}

}
