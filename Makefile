
STEST=go test -short
TEST=go test
TESTPKGS=./lexer ./preparser ./parser
LINTPKGS=./token ./lexer ./preparser ./parser

all: dep test lint fmt build

test: 
	go test ${TESTPKGS}

test-short: dep
	go test -short ${TESTPKGS}

dep:
	go get -v -d ./...


lint:
	golint ./... 

fmt:
	gofmt -w .

build: dep
	go build ./cmd/inlinear
