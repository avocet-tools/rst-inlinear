package parser

import (
	"gitlab.com/avocet-tools/ast"
	"gitlab.com/avocet-tools/inlinear/preparser"
	"gitlab.com/avocet-tools/inlinear/token"
	"gitlab.com/avocet-tools/log"
	"strings"
)

// Parser struct provides methods to perform an inlinear parse on reStructuredText.
type Parser struct {
	l  *preparser.Preparser
	es []*ast.Element

	prime   *token.Token
	secunde *token.Token
	terte   *token.Token
	quarte  *token.Token
	quinte  *token.Token

	Trace func(...any)
	Debug func(...any)
	Info  func(...any)
	Warn  func(...any)
	Error func(...any)
	Fatal func(...any)

	Tracef func(string, ...any)
	Debugf func(string, ...any)
	Infof  func(string, ...any)
	Warnf  func(string, ...any)
	Errorf func(string, ...any)
	Fatalf func(string, ...any)
}

// New takes a filename, a raw string, and a line number and returns a pointer for the
// inlinear parser.
func New(fname, raw string, line int) *Parser {
	p := &Parser{
		l:  preparser.New(fname, raw, line),
		es: []*ast.Element{},
	}
	p.prime = p.l.Next()
	p.secunde = p.l.Next()
	p.terte = p.l.Next()
	p.quarte = p.l.Next()
	p.quinte = p.l.Next()

	lgr := log.New("inlinear", "inlinear:parser")
	p.Trace, p.Tracef = lgr.Trace()
	p.Debug, p.Debugf = lgr.Debug()
	p.Info, p.Infof = lgr.Info()
	p.Warn, p.Warnf = lgr.Warn()
	p.Error, p.Errorf = lgr.Error()
	p.Fatal, p.Fatalf = lgr.Fatal()

	p.Debugf("Instantiated parser for %s:%d", fname, line)
	return p
}

func (p *Parser) read() {
	p.prime = p.secunde
	p.secunde = p.terte
	p.terte = p.quarte
	p.quarte = p.quinte
	p.quinte = p.l.Next()
}

// Parse method performs syntactic analysis and returns an array of inlinear AST elements.
func (p *Parser) Parse() []*ast.Element {

	for p.prime.Type != token.EOL {
		p.analyze()
		p.read()
	}

	// Postparse Sequential Text
	es := []*ast.Element{}
	size := len(p.es)
	pos := 0
	for pos < size {
		e := p.es[pos]
		if e.Type == ast.Text && pos+1 < size {
			text := []string{e.Text}
			for pos+1 < size {
				f := p.es[pos+1]
				if f.Type != ast.Text {
					break
				}
				text = append(text, f.Text)
				pos++
			}
			e.Text = strings.Join(text, "")
		}
		es = append(es, e)
		pos++
	}

	return es
}

func (p *Parser) add(e *ast.Element) {
	p.es = append(p.es, e)
}
