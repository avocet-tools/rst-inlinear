package parser

import (
	"gitlab.com/avocet-tools/ast"
	"gitlab.com/avocet-tools/inlinear/token"
)

func (p *Parser) analyze() {
	switch p.prime.Type {
	case token.Text, token.Space:
		p.parseText()
	case token.DStar:
		p.parseMarkup(ast.Strong, token.DStar, "Unclosed double star (**)")
	case token.Star:
		p.parseMarkup(ast.Emphasis, token.Star, "Unclosed star (*)")
	case token.DTick:
		p.parseMarkup(ast.Code, token.DTick, "Unclosed double tick (``)")
	case token.Tick:
		p.parseLink()
	case token.DOBrack:
		p.parseMarkup(ast.End, token.DCBrack, "Unclosed double bracket (]])")
	case token.OBrack:
		p.parseScoreClosers(brackClosers, brackTypes, "Unclosed bracket (])")
	case token.DBar:
		p.parseMarkup(ast.Replace, token.DBar, "Unclosed double bar (||)")
	case token.Bar:
		p.parseScoreClosers(barClosers, barTypes, "Unclosed bar (|)")
	case token.Colon:
		p.parseRole()
   default:
      p.Warnf("Unknown Token: %q", token.Pretty(p.prime.Type))
	}
}
