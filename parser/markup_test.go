package parser

import (
	"gitlab.com/avocet-tools/ast"
	"testing"
)

func Test_markup(t *testing.T) {
	name := "rst/inlinear/parser.Test_markup"
	data := []struct {
		raw   string
		text  string
		key   string
		atype ast.EType
	}{
		{"Text", "Text", "", ast.Text},
		{"Text space", "Text space", "", ast.Text},
		{"Text  dspace", "Text dspace", "", ast.Text},
		{"**Bold Text**", "Bold Text", "", ast.Strong},
		{"*Ital Text*", "Ital Text", "", ast.Emphasis},
		{"``Code Text``", "Code Text", "", ast.Code},
		{"`Literal`", "Literal", "", ast.Literal},
		{"`key`_", "key", "key", ast.VLink},
		{"`key`__", "key", "key", ast.ULink},
		{"`Title <key>`_", "Title", "key", ast.VLink},
		{"`Title <key>`__", "Title", "key", ast.ULink},
		{"[[Note]]", "", "Note", ast.Endnote},
		{"[Note]", "", "Note", ast.Footnote},
		{"[Cite]_", "", "Cite", ast.VCite},
		{"[Cite]__", "", "Cite", ast.UCite},
		{"||Sub||", "", "Sub", ast.Replace},
		{"|Sub|", "", "Sub", ast.Sub},
		{"|Sub|_", "", "Sub", ast.VLinkSub},
		{"|Sub|__", "", "Sub", ast.ULinkSub},
	}

	for pos, datum := range data {
		p := New("markup_test.go", datum.raw, pos)
		es := p.Parse()

		size := len(es)
		if size != 1 {
			for _, e := range es {
				t.Logf("EType: %s", e.Text)
			}
			t.Fatalf(
				"%s %d: parser returned wrong elements: %d != 1",
				name, pos, size)
		}
		e := es[0]
		if e.Text != datum.text {
			t.Logf("EType: %s", ast.PrettyE(e.Type))
			t.Fatalf(
				"%s %d: parser set wrong text: %q != %q",
				name, pos, e.Text, datum.text)
		}

		if e.Line != pos {
			t.Fatalf(
				"%s %d: parser set wrong line number: %d != %d",
				name, pos, e.Line, pos)
		}

		if e.Pos != 1 {
			t.Fatalf(
				"%s %d: parser set wrong position: %d != %d",
				name, pos, e.Pos, 1)
		}

		if e.Type != datum.atype {
			t.Fatalf(
				"%s %d: parser set wrong element type: %q != %q",
				name, pos, ast.PrettyE(e.Type), ast.PrettyE(datum.atype))
		}
		if e.Key != datum.key {
			t.Fatalf(
				"%s %d: parser set wrong key: %q != %q",
				name, pos, e.Key, datum.key)
		}
	}
}
