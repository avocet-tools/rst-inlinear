package parser

import (
	"regexp"
	"strings"

	"gitlab.com/avocet-tools/ast"
	"gitlab.com/avocet-tools/inlinear/token"
)

func (p *Parser) parseText() {
	e := ast.NewElement(ast.Text, p.prime.Line, p.prime.Pos)
	raw := []string{p.prime.Text}
	for p.secunde.Type == token.Text || p.secunde.Type == token.Space {
		raw = append(raw, p.secunde.Text)
		p.read()
	}
	e.Text = strings.Join(raw, "")
	p.add(e)
}

func (p *Parser) parseMarkup(atype ast.EType, ctype token.Type, msg string) {
	e := ast.NewElement(atype, p.prime.Line, p.prime.Pos)
	raw := []string{}
	for p.secunde.Type != ctype && p.prime.Type != token.EOL {
		raw = append(raw, p.secunde.Text)
		p.read()
	}

	if p.prime.Type == token.EOL {
		p.Warnf("%s:%d.%d - %s", p.prime.File, e.Line, e.Pos, msg)
		return
	}

	text := strings.Join(raw, "")
	if ctype == token.DCBrack || ctype == token.DBar {
		e.Key = text
	} else {
		e.Text = text
	}
   p.read()
	p.add(e)
}

func (p *Parser) nextIsClose(ctypes []token.Type) bool {
	for _, t := range ctypes {
		if p.secunde.Type == t {
			return true
		}
	}
	return p.prime.Type != token.EOL
}

func (p *Parser) parseClose(m map[token.Type]ast.EType, ctypes []token.Type, msg string) {
	line := p.prime.Line
	pos := p.prime.Pos
	raw := []string{}

	for !p.nextIsClose(ctypes) {
		raw = append(raw, p.secunde.Text)
		p.read()
	}

	if p.prime.Type == token.EOL {
		p.Warnf("%s:%d.%d - %s", p.prime.File, line, pos, msg)
		return
	}
	atype, ok := m[p.secunde.Type]
	if !ok {
		p.Warnf("%s:%d.%d - Invlaid closing token %q", p.prime.File, line, pos, token.Pretty(p.secunde.Type))
		return
	}
	e := ast.NewElement(atype, line, pos)
	e.Key = strings.Join(raw, "")
   p.read()
	p.add(e)
}

var tickClosers = []token.Type{
	token.Tick,
	token.TickDScore,
	token.TickScore,
}

var braceClosers = []token.Type{
	token.BraceTick,
	token.BraceTickDScore,
	token.BraceTickScore,
}

func (p *Parser) nextIsTickCloser() bool {
	return p.nextIsCloser(tickClosers)
}

func (p *Parser) nextIsBraceCloser() bool {
	return p.nextIsCloser(braceClosers)
}

func (p *Parser) nextIsCloser(ts []token.Type) bool {
	for _, closer := range ts {
		if p.secunde.Type == closer {
			return true
		}
	}
	return false
}

var braceSplit = regexp.MustCompile("<")

func (p *Parser) parseLink() {
	line := p.prime.Line
	pos := p.prime.Pos
	raw := []string{}

	for (!p.nextIsTickCloser() && !p.nextIsBraceCloser()) && p.prime.Type != token.EOL {
		raw = append(raw, p.secunde.Text)
		p.read()
	}

	if p.prime.Type == token.EOL {
		p.Warnf("%s:%d.%d - Unclosed tick", p.prime.File, line, pos)
		return
	}
	text := strings.Join(raw, "")
	key := text
	atype := ast.Literal

	// Close Brace with separate Key and Text
	if p.secunde.Type == token.BraceTickScore || p.secunde.Type == token.BraceTickDScore {
		res := braceSplit.Split(text, 2)
		if len(res) == 2 {
			text = strings.TrimSpace(res[0])
			key = strings.TrimSpace(res[1])
		}
	}

	switch p.secunde.Type {
	case token.BraceTickDScore, token.TickDScore:
		atype = ast.ULink
	case token.BraceTickScore, token.TickScore:
		atype = ast.VLink
	case token.BraceTick, token.Tick:
		key = ""
	}
	p.read()

	e := ast.NewElement(atype, line, pos)
	e.Text = text
	e.Key = key
	p.add(e)
}

var (
	brackClosers = []token.Type{
		token.CBrack,
		token.CBrackScore,
		token.CBrackDScore,
	}
	brackTypes = map[token.Type]ast.EType{
		token.CBrack:       ast.Foot,
		token.CBrackScore:  ast.VCite,
		token.CBrackDScore: ast.UCite,
	}

	barClosers = []token.Type{
		token.Bar,
		token.BarScore,
		token.BarDScore,
	}
	barTypes = map[token.Type]ast.EType{
		token.Bar:       ast.Sub,
		token.BarScore:  ast.VLinkSub,
		token.BarDScore: ast.ULinkSub,
	}
)

func (p *Parser) parseScoreClosers(ts []token.Type, atypes map[token.Type]ast.EType, msg string) {
	line := p.prime.Line
	pos := p.prime.Pos
	raw := []string{}

	for !p.nextIsCloser(ts) && p.prime.Type != token.EOL {
		raw = append(raw, p.secunde.Text)
		p.read()
	}

	if p.prime.Type == token.EOL {
		p.Warnf("%s:%d.%d - %s", p.prime.File, line, pos, msg)
		return
	}
	atype, ok := atypes[p.secunde.Type]
	if !ok {
		p.Warnf(
			"%s:%d.%d - Score closer ended on invalid type: %q",
			p.prime.File, line, pos, token.Pretty(p.secunde.Type))
		return
	}
	e := ast.NewElement(atype, line, pos)
	e.Key = strings.Join(raw, "")
	p.add(e)
}
