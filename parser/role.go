package parser

import (
	"fmt"
	"gitlab.com/avocet-tools/ast"
	"gitlab.com/avocet-tools/inlinear/token"
	"strings"
)

func (p *Parser) roleNameEnd(tok *token.Token) bool {
	return tok.Type == token.ColonTick || tok.Type == token.ColonTickTilda
}

func (p *Parser) parseRole() {
	line := p.prime.Line
	pos := p.prime.Pos
	if p.secunde.Type == token.Text {
		if p.roleNameEnd(p.terte) {
			role := p.secunde.Text
			p.read()
			p.read()
			p.parseRoleContent("", role, line, pos)
			return
		} else if p.roleNameEnd(p.quinte) {
			dom := p.secunde.Text
			role := p.quarte.Text
			p.read()
			p.read()
			p.read()
			p.read()
			p.parseRoleContent(dom, role, line, pos)
			return
		}
	}
	p.parseText()
}

func (p *Parser) nextIsRoleCloser() bool {
	return p.secunde.Type == token.Tick || p.secunde.Type == token.BraceTick
}

func (p *Parser) parseRoleContent(dom, role string, line, pos int) {
	tilda := p.prime.Type == token.ColonTickTilda
	raw := []string{}

	for !p.nextIsRoleCloser() && p.prime.Type != token.EOL {
		raw = append(raw, p.secunde.Text)
		p.read()
	}

	if p.prime.Type == token.EOL {
		name := fmt.Sprintf("%s:%s", dom, role)
		if dom == "" {
			name = role
		}
		p.Warnf("%s:%d.%d - Unclosed role(%s)", p.prime.File, line, pos, name)
		return
	}
	text := strings.Join(raw, "")
	key := text

	if tilda {
		res := strings.Split(text, ".")
		text = res[len(res)-1]
	} else if p.secunde.Type == token.BraceTick {
		res := braceSplit.Split(text, 2)
		if len(res) == 2 {
			text = strings.TrimSpace(res[0])
			key = strings.TrimSpace(res[1])
		}
	}
	e := ast.NewElement(ast.Role, line, pos)
	e.Text = text
	e.Key = key
	e.Dom = dom
	e.Role = role
   p.read()
	p.add(e)
}
