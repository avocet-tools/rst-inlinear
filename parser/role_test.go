package parser

import (
	"gitlab.com/avocet-tools/ast"
	"testing"
)

func Test_roles(t *testing.T) {
	name := "rst/inlinear/parser.Test_roles"
	data := []struct {
		raw  string
		text string
		key  string
		dom  string
		role string
	}{
		{":ref:`config`", "config", "config", "", "ref"},
		{":ref:`Title <config>`", "Title", "config", "", "ref"},
		{":rst:ref:`config`", "config", "config", "rst", "ref"},
		{":rst:ref:`Title <config>`", "Title", "config", "rst", "ref"},
		{":ref:`~parent.config`", "config", "parent.config", "", "ref"},
		{":rst:ref:`~parent.config`", "config", "parent.config", "rst", "ref"},
	}

	for pos, datum := range data {
		p := New("role_test.go", datum.raw, pos)
		es := p.Parse()

		size := len(es)
		if size != 1 {
			t.Fatalf(
				"%s %d: parser returned wrong elements: %d != %d",
				name, pos, size, 1)
		}

		e := es[0]
		if e.Type != ast.Role {
			t.Fatalf(
				"%s %d: parser returned element of wrong type: %q != %q",
				name, pos, ast.PrettyE(e.Type), ast.PrettyE(ast.Role))
		}

		if e.Text != datum.text {
			t.Fatalf(
				"%s %d: parser returned wrong text: %q != %q",
				name, pos, e.Text, datum.text)
		}

		if e.Key != datum.key {
			t.Fatalf(
				"%s %d: parser returned wrong key: %q != %q",
				name, pos, e.Key, datum.key)
		}

		if e.Dom != datum.dom {
			t.Fatalf(
				"%s %d: parser returned wrong domain: %q != %q",
				name, pos, e.Dom, datum.dom)
		}

		if e.Role != datum.role {
			t.Fatalf(
				"%s %d: parser returned wrong domain: %q != %q",
				name, pos, e.Role, datum.role)
		}
	}
}
