package inlinear

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/avocet-tools/ast"
	"gitlab.com/avocet-tools/inlinear/parser"
)

// RunParse parses the given string as called from the command-line
func RunParse(args []string) {
	initLogger()
   info("Called parse operation")
	start := time.Now()
	es := Parse("stdin", strings.Join(args, " "), 1)

   debug("Marshaling Elements to JSON")
	data, err := json.MarshalIndent(es, " ", " ")
	if err != nil {
		fatal(err)
	}
	fmt.Println(string(data))
	fmt.Println("Parse Complete: ", len(es), " in ", time.Since(start))

}

// Parse returns takes a filename, raw string, and line number and returns a parsed AST for
// inlinear elements.
func Parse(fname, raw string, line int) []*ast.Element {
	p := parser.New(fname, raw, line)
	return p.Parse()
}
