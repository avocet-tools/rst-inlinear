package token

// Type sets the result of lexical analysis on the token.
type Type int

// Specifies constants for each type available.
const (
	Unset Type = iota
	EOL
	Text
	DStar
	Star
	DTick
	Tick
	TickDScore
	TickScore
	BraceTick
	BraceTickDScore
	BraceTickScore
	Colon
	ColonTick
	ColonTickTilda
	ColonSlashSlash
	DOBrack
	OBrack
	CBrack
	CBrackDScore
	CBrackScore
	DCBrack
	DBar
	Bar
	BarDScore
	BarScore
	NL
	Space
)

// Pretty takes a type value and returns a string.  This is mainly used in
// logging and testing functions
func Pretty(t Type) string {
	switch t {
	case Unset:
		return "unset-token"
	case EOL:
		return "eol-token"
	case Text:
		return "text-token"
	case DStar:
		return "double-start-token"
	case Star:
		return "star-token"
	case DTick:
		return "double-tick-token"
	case Tick:
		return "tick-token"
	case TickDScore:
		return "tick-double-score-token"
	case TickScore:
		return "tick-score-token"
	case BraceTick:
		return "brace-tick-token"
	case BraceTickDScore:
		return "brace-tick-double-score-token"
	case BraceTickScore:
		return "brace-tick-score-token"
	case Colon:
		return "colon-token"
	case ColonTick:
		return "colon-tick-token"
	case ColonTickTilda:
		return "colon-tick-tilda-token"
	case ColonSlashSlash:
		return "colon-slash-slash-token"
	case DOBrack:
		return "double-open-bracket-token"
	case OBrack:
		return "open-bracket-token"
	case CBrack:
		return "closed-bracket-token"
	case CBrackDScore:
		return "closed-bracket-double-score-token"
	case CBrackScore:
		return "closed-bracket-score-token"
	case DCBrack:
		return "double-closed-bracket-token"
	case DBar:
		return "double-bar-token"
	case Bar:
		return "bar-token"
	case BarDScore:
		return "bar-double-score-token"
	case BarScore:
		return "bar-score-token"
	case NL:
		return "newline-token"
	case Space:
		return "space-token"
	default:
		return "unspecified-tokne"
	}

}
