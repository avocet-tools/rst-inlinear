// Package token provides basic structs and functions used by the lexer to
// analyze inlinear reStructuredText.
package token

// Token struct stores information on an element of reStrucutredText found
// during lexical analysis.
type Token struct {
	Type Type   `json:"token_type"`
	File string `json:"token_name"`
	Line int    `json:"lineno"`
	Pos  int    `json:"position"`

	Raw  string `json:"raw"`
	Text string `json:"text"`
}

// New takes a filename for logging purposes and a line number and position and
// returns an Token pointer with no specified text values.
func New(fname string, line, pos int) *Token {
	return &Token{
		File: fname,
		Line: line,
		Pos:  pos,
	}
}

// Init takes a raw string and initializes the Raw and Text fields.
func (tok *Token) Init(raw string) {
	tok.Raw = raw
	tok.Text = raw
}
