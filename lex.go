package inlinear

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/avocet-tools/inlinear/lexer"
	"gitlab.com/avocet-tools/inlinear/token"
)

// RunLex performs lexical analysis when called from CLI.
func RunLex(args []string) {
	initLogger()
	info("Called lex operation")
	start := time.Now()
	toks := Lex("stdin", strings.Join(args, " "), 1)

	debug("Marshaling Tokens into JSON")
	data, err := json.MarshalIndent(toks, " ", " ")
	if err != nil {
		fatal(err)
	}
	trace("Marshal complete")
	fmt.Println(string(data))
	fmt.Println("Lexical Analysis Complete: ", len(toks), " in ", time.Since(start))
}

// Lex takes a filename, a reStructuredText string and a line number and returns an array
// of tokens resulting from the lexical analysis.
func Lex(fname, raw string, line int) []*token.Token {
	tracef("Lexing %d char string", len(raw))
	l := lexer.New(fname, raw, line)
	toks := []*token.Token{}
	tok := l.Next()

	for tok.Type != token.EOL {
		toks = append(toks, tok)
		tok = l.Next()
	}
	toks = append(toks, l.Next())

	return toks
}
